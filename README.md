GraphicsDevicePolicyFixup
===================

An open source kernel extension

#### Installation
- Must install Lilu.kext (make sure Release/Debug config match Lilu extension type)

#### Compile
- The Acidanthera SDK is required to compile. Execute the following command in the resource folder.
```sh
git clone https://github.com/acidanthera/MacKernelSDK
```
#### Features
- Disabling AppleGraphicsDevicePolicy check

#### How it works
 - Remove board-id in AppleGraphicsDevicePolicy

#### Credits
- [Apple](https://www.apple.com) for macOS  
- [vit9696](https://github.com/vit9696) for [Lilu.kext](https://github.com/vit9696/Lilu) & for zero-length string comparison patch (AppleGraphicsDevicePolicy.kext )
- [Pike R. Alpha](https://github.com/Piker-Alpha) for board-id patch (AppleGraphicsDevicePolicy.kext)
- [FredWst](http://www.insanelymac.com/forum/user/509660-fredwst/)
- [igork](https://applelife.ru/members/igork.564) for adding properties IOVARendererID & IOVARendererSubID in nvAcceleratorParent::SetAccelProperties
- [lvs1974](https://applelife.ru/members/lvs1974.53809) for writing the software and maintaining it
- [jyavenard]( https://github.com/jyavenard/Vega5KFixup) for update the software and maintaining it
