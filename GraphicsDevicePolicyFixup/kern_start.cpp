//
//  kern_start.cpp
//  GDPFixup
//
//  Copyright © 2017 - 2019 PMheart. All rights reserved.
//

#include <Headers/plugin_start.hpp>
#include <Headers/kern_api.hpp>

#include "GDPFixup.hpp"

static GDPFixupPlugin gdpf;

static const char *bootargOff[] {
	"-gdpfoff"
};

static const char *bootargDebug[] {
	"-gdpfdbg"
};

static const char *bootargBeta[] {
	"-gdpfbeta"
};

PluginConfiguration ADDPR(config) {
	xStringify(PRODUCT_NAME),
	parseModuleVersion(xStringify(MODULE_VERSION)),
	LiluAPI::AllowNormal | LiluAPI::AllowInstallerRecovery | LiluAPI::AllowSafeMode,
	bootargOff,
	arrsize(bootargOff),
	bootargDebug,
	arrsize(bootargDebug),
	bootargBeta,
	arrsize(bootargBeta),
	KernelVersion::MountainLion,
	KernelVersion::BigSur,
	[]() {
		gdpf.init();
	}
};
