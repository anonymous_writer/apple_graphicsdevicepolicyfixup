//
//  GDPFixup.cpp
//  GraphicsDevicePolicyFixup based on NvidiaGraphicsFixup
//
//  Copyright © 2017 lvs1974. All rights reserved.
//  Copyright © 2018-2019 JYA. All rights reserved.
//

#include <Headers/kern_api.hpp>
#include <Headers/kern_util.hpp>
#include <Headers/kern_iokit.hpp>

#include "kern_config.hpp"
#include "GDPFixup.hpp"


static const char *kextAGDPolicy[] { "/System/Library/Extensions/AppleGraphicsControl.kext/Contents/PlugIns/AppleGraphicsDevicePolicy.kext/Contents/MacOS/AppleGraphicsDevicePolicy" };
static const char *kextAGDPolicyId { "com.apple.driver.AppleGraphicsDevicePolicy" };

static const char *kextRadeon[] { "/System/Library/Extensions/Radeon.kext/Contents/MacOS/Radeon" };
static const char *kextRadeonId { "com.apple.Radeon" };

static KernelPatcher::KextInfo kextList[] {
    { kextAGDPolicyId,      kextAGDPolicy,   arrsize(kextAGDPolicy),  {true}, {}, KernelPatcher::KextInfo::Unloaded },
    { kextRadeonId,        kextRadeon,     arrsize(kextRadeon),    {},     {}, KernelPatcher::KextInfo::Unloaded },
};

static size_t kextListSize {arrsize(kextList)};

// Only used in apple-driven callbacks
static GDPFixupPlugin *callbackGDPFixupPlugin = nullptr;


bool GDPFixupPlugin::init() {
    LiluAPI::Error error = lilu.onKextLoad(kextList, kextListSize,
        [](void *user, KernelPatcher &patcher, size_t index, mach_vm_address_t address, size_t size) {
            callbackGDPFixupPlugin = static_cast<GDPFixupPlugin *>(user);
            callbackGDPFixupPlugin->processKext(patcher, index, address, size);
        }, this);
    
    if (error != LiluAPI::Error::NoError) {
        SYSLOG("gdpf", "failed to register onKextLoad method %d", error);
        return false;
    }
    
    return true;
}

void GDPFixupPlugin::deinit() {
}

void GDPFixupPlugin::processKext(KernelPatcher &patcher, size_t index, mach_vm_address_t address, size_t size) {
    if (progressState != ProcessingState::EverythingDone) {
        for (size_t i = 0; i < kextListSize; i++) {
            if (kextList[i].loadIndex == index) {
                if (!(progressState & ProcessingState::GraphicsDevicePolicyPatched) && !strcmp(kextList[i].id, kextAGDPolicyId))
                {
                    DBGLOG("gdpf", "found %s", kextAGDPolicyId);

                    const uint8_t find[]    = "board-id";
                    const uint8_t replace[] = "board-ix";
                    KextPatch kext_patch {
                        {&kextList[i], find, replace, strlen((const char*)find), 1},
                        KernelVersion::MountainLion, KernelPatcher::KernelAny
                    };

                    applyPatches(patcher, index, &kext_patch, 1, "pikera");

                    progressState |= ProcessingState::GraphicsDevicePolicyPatched;
                }
                else if (!(progressState & ProcessingState::RadeonRouted) && !strcmp(kextList[i].id, kextRadeonId))
                {
                    DBGLOG("gdpf", "found %s", kextRadeonId);
                    auto method_address = patcher.solveSymbol(index, "__ZN13nvAccelerator18SetAccelPropertiesEv");
                    if (method_address) {
                        DBGLOG("ngfx", "obtained __ZN13nvAccelerator18SetAccelPropertiesEv");
                        patcher.clearError();
                        orgSetAccelProperties = reinterpret_cast<t_set_accel_properties>(patcher.routeFunction(method_address, reinterpret_cast<mach_vm_address_t>(nvAccelerator_SetAccelProperties), true));
                        if (patcher.getError() == KernelPatcher::Error::NoError) {
                            DBGLOG("gdpf", "routed __ZN13nvAccelerator18SetAccelPropertiesEv");
                        } else {
                            SYSLOG("gdpf", "failed to route __ZN13nvAccelerator18SetAccelPropertiesEv");
                        }
                    } else {
                        SYSLOG("gdpf", "failed to resolve __ZN13nvAccelerator18SetAccelPropertiesEv");
                    }

                    progressState |= ProcessingState::RadeonRouted;
                }
             }
        }
    }
    
    // Ignore all the errors for other processors
    patcher.clearError();
}

void GDPFixupPlugin::nvAccelerator_SetAccelProperties(IOService* that)
{
    DBGLOG("gdpf", "SetAccelProperties is called");
}


void GDPFixupPlugin::applyPatches(KernelPatcher &patcher, size_t index, const KextPatch *patches, size_t patchNum, const char* name) {
    DBGLOG("gdpf", "applying patch '%s' for %zu kext", name, index);
    for (size_t p = 0; p < patchNum; p++) {
        auto &patch = patches[p];
        if (patch.patch.kext->loadIndex == index) {
            if (patcher.compatibleKernel(patch.minKernel, patch.maxKernel)) {
                DBGLOG("gdpf", "applying %zu patch for %zu kext", p, index);
                patcher.applyLookupPatch(&patch.patch);
                // Do not really care for the errors for now
                patcher.clearError();
            }
        }
    }
}
