GraphicsDevicePolicyFixup Changelog
=============================
#### v1.0.4
- Improvements made by [noirosx](https://www.hackintosh-forum.de/user/40078-noirosx/) 

#### v1.0.3
- Troubleshooting

#### v1.0.1
- Update to latest Lilu.kext 1.5.0
- Change to Acidanthera SDK
- Remove NVidia Settings

#### v1.0.0
- First version

